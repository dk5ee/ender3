# Ender3
Supercharge your Ender3 with Guides, Mods and Addons.

Make your Ender3 work and worth like high end machines.

**Original Source** https://github.com/sn4k3/Ender3

**Note:** The Creator "sn4k3" has spend many hours trying put this together so we can supercharge our Ender-3 for "Free", if sn4ke's work is important to you or you fell this worth a tip sn4k3 is open to donations: https://www.paypal.me/SkillTournament

Link to Facebook deleted.

**NOTE:** Fine tune the start gcode for your printer! This is very important since i can't provide a all case profile for every modification and printer.

Modify at your needs.

## Installation
1. Download Slic3r with apt install slic3r
2. Copy GitHub Slic3r subfolders ("printer", "filament", "print") to the config folder of slic3r
3. Open Slic3r and check if profiles are there
4. To clean up interface remove profiles that will not be used: Printers you will not use (RECOMMENDED)
5. Go under printer and use your current values for your printer, like max Z, limits, start gcodes or other things you may need to tune
6. Do a simple test print

