Notes for Creality Ender 3

The 3d printer only accepts gcode, using a marlin firmware to interpret that code into stepper motor movements.

Supported gcodes can be be found here: 
http://marlinfw.org/meta/gcode/

The printer comes with a microSD card with a sample gcode. Use this for comparison with the output of the following software

## first try with slic3r:

generating gcode with slic3r:

1. apt-get install slic3r
2. setup the configuration. I've exported my first slic3r settings as slic3r_config.ini into this repository
3. open STL file in slic3r
4. export gcode to file, put that on microSD card
5. run to printer, insert, print.

## using Cura:

the problem with cura:

* Latest Version is 4.x
* Cura only supports ender3 from stock from Version 3.5 or so
* The Version in debian repositories did not support ender3
* octoprint uses cura-engine 15.x (old versioning scheme) for slicing. Newer Cura versions changed the API, and the configs are not usable
* I found a still untested config for ender3 on github: Ender3Cura15.ini 

## using octoprint:

octoprint with raspberry pi - still working on that

1. obtain raspi, microsd, usb power supply, webcam
2. set up octopi on microsd card: https://octoprint.org/download/
3. enable ssh, change password, test connection and configure wifi with raspi-config
4. connect all together and power up. ender3 is detected automaticly
5. octoprint can slice stls directly with cura-engine or print gcode directly. Simply upload the STL or gcode
6. on "Control" an attached webcam can be viewed. Webcam is also streamed on port 8080

## better slic3r:

I found this repo with nice settings for slicerPE: https://github.com/sn4k3/Ender3

These are settings for the slic3er fork "slicer pruse edition", which are slightly incompatible with normal slic3r.

There are corrected settings in the folder Slic3r/ for slic3r 1.3.0

## Designing:

* Just use STLs from https://www.thingiverse.com/ or https://www.yeggi.com/ (last one is search engine for 3d files)
* OpenSCAD - bestest 3d CAD software for programmers. In this repo is a testding.scad, the generated testding.stl, and a slic3d testding.gcode

