
echo(version=version());

module example_intersection()
{
 
	intersection() {
		difference() {
			union() {
				cube([30, 30, 30], center = true);
				translate([0, 0, -25])
					cube([15, 15, 50], center = true);
			}
			union() {
				cube([50, 10, 10], center = true);
				cube([10, 50, 10], center = true);
				cube([10, 10, 50], center = true);
			}
		}
		translate([0, 0, 10])
			cylinder(h = 50, r1 = 20, r2 = 5, center = true);
	}

}
union() {
       translate([10, 15.2, 15]) {
example_intersection();
       };
    rotate ([90,0,0]) linear_extrude(height = 2, center = true) {text("one");};
}